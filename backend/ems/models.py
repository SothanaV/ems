from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=254)
    image = models.ImageField()
    is_staff = models.BooleanField(default=False)

    def __str__(self):
        return "{} | {}".format(self.user.username, self.first_name)
    
class Category(models.Model):
    name = models.CharField(max_length=20)
    is_use = models.BooleanField()

    def __str__(self):
        return self.name

class Building(models.Model):
    name = models.CharField(max_length=10)
    is_use = models.BooleanField()

    def __str__(self):
        return self.name

class Room(models.Model):
    name = models.CharField(max_length=3)
    building = models.ForeignKey(Building, on_delete=models.PROTECT)
    in_use = models.BooleanField(default=False)
    is_use = models.BooleanField()

    def __str__(self):
        return "{} | {}".format(self.building.name, self.name)

class Equipment(models.Model):
    code = models.CharField(max_length=20, unique=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    room = models.ForeignKey(Room, on_delete=models.PROTECT)
    add_date = models.DateField(auto_now_add=True)
    STATUS_CHOICES = [
        ('A',"Avaliable"),
        ('C','Checkout'),
        ('F','Fix'),
        ('R','Ruin')
    ]
    status = models.CharField(choices=STATUS_CHOICES, max_length=1, default='A')

class Transactions(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    start = models.DateTimeField(auto_now_add=True)
    stop = models.DateTimeField(null=True, blank=True)
    used_time = models.PositiveSmallIntegerField(null=True, blank=True)
    is_complete = models.BooleanField(default=False)
