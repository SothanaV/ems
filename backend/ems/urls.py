from django.urls import path
from .views import genRoomQrCode, genEquipCode
urlpatterns = [
    path('qr-room', genRoomQrCode, name="qr-room"),
    path('qr-equip', genEquipCode, name="qr-equip"),
]
