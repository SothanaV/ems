from django.contrib import admin
from .models import Account, Category, Building, Room, Equipment, Transactions
# Register your models here.
class AccountAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Account._meta.fields]
admin.site.register(Account, AccountAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Category._meta.fields]
admin.site.register(Category, CategoryAdmin)

class BuildingAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Building._meta.fields]
    list_editable = ['is_use']
admin.site.register(Building, BuildingAdmin)

class BuildingAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Room._meta.fields]
admin.site.register(Room, BuildingAdmin)

class EquipmentAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Equipment._meta.fields]
admin.site.register(Equipment, EquipmentAdmin)

class TransactionsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Transactions._meta.fields]
admin.site.register(Transactions, TransactionsAdmin)