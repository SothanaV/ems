from django.shortcuts import render
from .models import Room, Equipment
# Create your views here.
def genRoomQrCode(request):
    room = Room.objects.filter(is_use=True).values('id','name','building__name')
    pages = []
    for i in range(0, len(room), 4):
        page = []
        for j in range(4):
            if i+j >= len(room):
                continue
            page.append(room[i+j])
        pages.append(page)
    ids = [x['id'] for x in room]
    context = {
        'pages':pages,
        'ids':ids
    }
    return render(request, 'roomQr.html', context)

def genEquipCode(request):
    equip = Equipment.objects.all().values('id', 'code', 'room__name','room__building__name')
    pages = []
    for i in range(0, len(equip), 4):
        page = []
        for j in range(4):
            if i+j >= len(equip):
                continue
            page.append(equip[i+j])
        pages.append(page)
    ids = [x['id'] for x in equip]
    context = {
        'pages':pages,
        'ids':ids
    }
    return render(request, 'equipQr.html', context)