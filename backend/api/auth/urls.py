from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from .viewsets import UserCreateViewsets, AccountViewsets

router = routers.DefaultRouter()
router.register(r'register',UserCreateViewsets, basename='createuser')
router.register(r'edit_account', AccountViewsets, basename='editaccount')

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(router.urls)),
]
