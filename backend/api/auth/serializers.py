from django.contrib.auth.models import User, Group
from rest_framework import serializers
from ems.models import Account

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['user',]

class UserRegisterSerializer(serializers.HyperlinkedModelSerializer):
    account = AccountSerializer(required=True)
    class Meta:
        model = User
        fields = ['username', 'password', 'account']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        account_data = validated_data.pop('account')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        Account.objects.create(user=user, **account_data)
        return user
    
class UserRegisterSerializer2(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username', 'password']
        extra_kwargs = {'password': {'write_only': True}, 'id':{'read_only':True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user
class AccountSerializer2(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['user',]