from django.contrib.auth.models import User
from rest_framework import viewsets, generics, mixins
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from .serializers import UserRegisterSerializer, AccountSerializer, UserRegisterSerializer2
from ems.models import Account

class UserCreateViewsets(viewsets.ModelViewSet):
    # queryset = User.objects.all()
    serializer_class = UserRegisterSerializer
    permission_classes = (AllowAny, )
    http_method_names = ['post', 'head']
    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return User.objects.filter(username=user.username)
        else:
            return []

class AccountViewsets(viewsets.ModelViewSet):
    serializer_class = AccountSerializer
    permission_classes = (IsAuthenticated, )
    # http_method_names = ['get', 'put' ,'head']
    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Account.objects.filter(user=user)
        else:
            return []
    def patch(self, request):
        instance = Account.objects.get(user=request.user)
        serializer = AccountSerializer(instance, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
