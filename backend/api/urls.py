from django.urls import path, include
from .v1.routers import router as router_v1
from .auth import urls as auth_url
urlpatterns = [
    path('v1/', include(router_v1.urls)),
    path('v1/auth/', include(auth_url)),
]