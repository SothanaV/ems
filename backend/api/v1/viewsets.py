import os
import pandas as pd
from django.db.models import Sum
from django.utils import timezone
from backend import settings
from rest_framework import viewsets, mixins, filters, status, generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import CategotySerializer, BuildingSerializer, RoomSerializer, EquipmentSerializer, \
                            TransactionsSerializer, ImportdataSerializer, AccountSerializer, ExportSerializer
from ems.models import Category, Building, Room, Equipment, Transactions, Account
from django.http import HttpResponse
from pyexcelerate import Workbook
class CategoryViewset(viewsets.GenericViewSet,
                        mixins.ListModelMixin):
    serializer_class = CategotySerializer
    permission_classes = [IsAuthenticated]
    model = Category

    def get_queryset(self):
        return self.model.objects.filter(is_use=True)

class BuildingViewset(viewsets.GenericViewSet,
                        mixins.ListModelMixin):
    serializer_class = BuildingSerializer
    permission_classes = [IsAuthenticated]
    model = Building

    def get_queryset(self):
        return self.model.objects.filter(is_use=True)

class RoomViewset(viewsets.GenericViewSet,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin):

    queryset = Room.objects.filter(is_use=True)
    serializer_class = RoomSerializer
    filter_backends = [filters.SearchFilter]
    permission_classes = [IsAuthenticated]
    search_fields = ['name']
    model = Room

class EquipmentViewset(viewsets.GenericViewSet,
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin):
    serializer_class = EquipmentSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend]
    search_fields = ['code']
    ordering_fields = ['code','category','add_date']
    permission_classes = [IsAuthenticated]
    model = Equipment

    def get_queryset(self):
        return self.model.objects.filter(status='A')
    
    def create(self, request):
        data = request.data
        try:
            self.model.objects.create(
                code = data['code'],
                category = Category.objects.get(id=data['category']),
                room = Room.objects.get(id=data['room'])
            )
        except Exception as e:
            res = {
                'error':str(e)
            }
            return Response(res, status=status.HTTP_400_BAD_REQUEST)
        else:
            res = {
                'detail': "Equipment added"
            }
            return Response(res, status=status.HTTP_201_CREATED)

class TransactionsViewset(viewsets.GenericViewSet,
                            mixins.ListModelMixin,
                            mixins.CreateModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.RetrieveModelMixin):
    serializer_class = TransactionsSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['is_complete']
    permission_classes = [IsAuthenticated]
    model = Transactions

    def get_queryset(self):
        return self.model.objects.all()
    
    def create(self, request):
        data = request.data
        print(data)
        try:
            room = Room.objects.get(id=data['room'])
            if 'state' in data:
                if data['state'] == 'in':
                    if room.in_use == False:
                        Transactions.objects.create(
                            room = room,
                        )
                        room.in_use = True
                        room.save()
                        res = {
                            'detail' : "transaction recorded",
                            'room' : str(room)
                        }
                        return Response(res, status=status.HTTP_201_CREATED)
                    else:
                        res = {
                            'detail' : "transaction this room exists"
                        }
                        return Response(res, status=status.HTTP_403_FORBIDDEN)
                elif data['state'] == 'out':
                    txn = Transactions.objects.filter(room__id=data['room'], room__in_use=True)
                    if txn.exists():
                        instance = txn.latest('id')
                        instance.stop = timezone.now()
                        instance.used_time = int((instance.stop - instance.start).total_seconds() / 60)
                        instance.is_complete = True
                        instance.save()

                        room = instance.room
                        room.in_use = False
                        room.save()
                        res = {
                            'detail': "Transaction checkouted"
                        }
                        return Response(res, status=status.HTTP_204_NO_CONTENT)
            else:
                res = {
                    'detail' : "require state"
                }
                return Response(res, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            res = {
                        'detail' : str(e)
            }
            return Response(res, status=status.HTTP_400_BAD_REQUEST)
    
    def partial_update(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.is_complete:
                res = {
                    'status': "Stop use eqiupment has been recorded" 
                }
                return Response(res, status=status.HTTP_403_FORBIDDEN)
            instance.stop = timezone.now()
            instance.used_time = int((instance.stop - instance.start).total_seconds() / 60)
            instance.is_complete = True
            instance.save()
            res = {
                'status': "Stop use eqiupment recorded" 
            }
            return Response(res, status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            res = {
                'status':e
            }
            return Response(res, status=status.HTTP_400_BAD_REQUEST) 

class ImportdataViewset(viewsets.ViewSet):
    extension = ['csv', 'xlsx', 'xls']
    serializer_class = ImportdataSerializer
    permission_classes = [IsAuthenticated]
    def create(self, request):
        data = request.data['file']
        ext = data.name.split('.')[-1].lower()
        print(ext)
        if ext not in self.extension:
            res = {
                'detail' : "File extension does not supported"
            }
            return Response(res, status=status.HTTP_400_BAD_REQUEST)
        df = None
        if ext == "csv":
            df = pd.read_csv(data, encoding='utf-8')
        elif ext in ["xlsx", "xls"]:
            df = pd.read_excel(data)
        category = {}
        for cat in Category.objects.filter(is_use=True):
            category[cat.name] = cat
        err = []
        for index, row in df.iterrows():
            try:
                Equipment.objects.create(
                    code = row.equipment_code,
                    category = category[row.categoty],
                    room = Room.objects.filter(name=row.room_no, building__name=row.building)[0],
                )
            except IndexError:
                err.append({
                    'detail': "Room doesn't exists",
                    'data': row.equipment_code
                })
            except Exception as e:
                err.append({
                    'detail': str(e),
                    'data': row.equipment_code
                })
        if len(err) != 0:
            res = {
                'status' : "Equipment Add sucessful but have some column wrong format",
                'error_data' : err
            }
        else:
            res = {
                'status' : "Equipment Add sucessful"
            }
        return Response(res, status=status.HTTP_201_CREATED)

class AccountViewset(viewsets.GenericViewSet,
                    mixins.RetrieveModelMixin,
                    mixins.ListModelMixin):
    serializer_class = AccountSerializer
    permission_classes = [IsAuthenticated]
    model = Account

    # def list(self, request, *args, **kwargs):
    #     user = request.user
    #     instance = self.model.objects.filter(user=user).values()[0]
    #     serializer = self.serializer_class(data=instance)
    #     if serializer.is_valid():
    #         return Response(serializer.data, status=status.HTTP_200_OK)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)
    
class ExportTransactionViewset(viewsets.ViewSet):
    serializer_class = ExportSerializer
    permission_classes = [IsAuthenticated]
    def list(self, request):
        transaction = Transactions.objects.filter(is_complete=True)
        year = []
        month = []
        for obj in transaction.values('stop').distinct():
            data = obj['stop']
            if data.year not in year:
                year.append(data.year)
            if data.month not in month:
                month.append(data.month)
        res = {
            'year':year,
            'month':month
        }
        return Response(res, status=status.HTTP_200_OK)
    
    def create(self, request):
        data = request.data
        f_name = "export_transaction_"
        transaction = Transactions.objects.filter(is_complete=True)
        if 'year' in data:
            transaction = transaction.filter(stop__year=data['year'])
            f_name += str(data['year'])
        if 'month' in data:
            transaction = transaction.filter(stop__month=data['month'])
            f_name += "_"+str(data['month'])
        df      = pd.DataFrame(transaction.values('room_id').annotate(total_time=Sum('used_time')))
        room    = pd.DataFrame(Room.objects.filter(is_use=True).values('name','id','building_id')).rename(columns={'name':'room'})
        building      = pd.DataFrame(Building.objects.filter(is_use=True).values('id','name')).rename(columns={'name':'building'})
        room_building = pd.merge(left=room, right=building, left_on='building_id', right_on='id')\
                            .drop(['id_y', 'building_id'], axis=1) \
                            .rename(columns={'id_x':'id'})
        outdata = df.merge(room_building, left_on='room_id', right_on='id').drop(['room_id','id'],axis=1).rename(columns={'total_time':'usedTime(min)'})
        # path_out_file = os.path.join(settings.MEDIA_ROOT, "{}.xlsx".format(f_name))
        response = HttpResponse(outdata.to_csv(index=False).encode('utf-8'), content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=' + "{}.csv".format(f_name)
        return response

class ExportEquipmentViewset(viewsets.ViewSet):
    serializer_class = ExportSerializer
    permission_classes = [IsAuthenticated]
    def list(self, request):
        equip = Equipment.objects.all()
        year = []
        month = []
        for obj in equip.values('add_date').distinct():
            data = obj['add_date']
            if data.year not in year:
                year.append(data.year)
            if data.month not in month:
                month.append(data.month)
        res = {
            'year':year,
            'month':month
        }
        return Response(res, status=status.HTTP_200_OK)
    
    def create(self, request):
        data = request.data
        f_name = "export_equipment_"
        equip = Equipment.objects.all()
        if 'year' in data:
            equip = equip.filter(add_date__year=data['year'])
            f_name += str(data['year'])
        if 'month' in data:
            equip = equip.filter(add_date__month=data['month'])
            f_name += "_"+str(data['month'])

        df = pd.DataFrame(equip\
             .values('code', 'category_id__name', 'room_id__name', 'room_id__building__name', 'add_date', 'status'))
        status_choice = pd.DataFrame({
            'key':[e[0] for e in Equipment.STATUS_CHOICES],
            'name':[e[1] for e in Equipment.STATUS_CHOICES]
        })
        outdata = pd.merge(left=df, right=status_choice, left_on='status', right_on='key')\
                    .drop(['status','key'], axis=1)\
                    .rename(columns={
                            'name':'status', 
                             'category_id__name':'category', 
                             'room_id__name':'room',
                            'room_id__building__name':'building'})
        # path_out_file = os.path.join(settings.MEDIA_ROOT, "{}.xlsx".format(f_name))
        response = HttpResponse(outdata.to_csv(index=False).encode('utf-8'), content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=' + "{}.csv".format(f_name)
        return response