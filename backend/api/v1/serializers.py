from rest_framework import serializers
from ems.models import Category, Building, Room, Equipment, Transactions, Account

class CategotySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields ='__all__'

class BuildingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = '__all__'

class RoomSerializer(serializers.ModelSerializer):
    building = serializers.StringRelatedField()
    class Meta:
        model = Room
        # fields = '__all__'
        exclude = ['is_use']

class EquipmentSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    category = serializers.StringRelatedField()
    class Meta:
        model = Equipment
        fields = '__all__'

    def get_status(self, obj):
        return obj.get_status_display()

class TransactionsSerializer(serializers.ModelSerializer):
    room = serializers.StringRelatedField()
    class Meta:
        model = Transactions
        fields = '__all__'

class ImportdataSerializer(serializers.Serializer):
    file = serializers.FileField()

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['user']

class ExportSerializer(serializers.Serializer):
    month = serializers.IntegerField()
    year = serializers.IntegerField()