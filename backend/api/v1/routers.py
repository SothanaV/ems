from rest_framework import routers
from .viewsets import CategoryViewset, BuildingViewset, RoomViewset, EquipmentViewset, TransactionsViewset, \
                        ImportdataViewset, AccountViewset, ExportTransactionViewset, ExportEquipmentViewset

router = routers.DefaultRouter()

router.register('categoty', CategoryViewset, basename='category')
router.register('building', BuildingViewset, basename='building')
router.register('room', RoomViewset, basename='room')
router.register('equipment', EquipmentViewset, basename='equipment')
router.register('transactions', TransactionsViewset, basename='transactions')
router.register('importdata', ImportdataViewset, basename='importdata')
router.register('account', AccountViewset, basename='account')
router.register('export-transaction',ExportTransactionViewset, basename='export-transaction')
router.register('export-equipment', ExportEquipmentViewset, basename='export-equipment')