from ems.models import Category, Building, Room

category = ['คอมพิวเตอร์', 'เครื่องใช้สำนักงาน']
for cat in category:
    Category.objects.update_or_create(
        name = cat,
        is_use = True,
    )

building = ['บร-2', 'บร-3', 'บร-4', 'บร-5']
for build in building:
    Building.objects.update_or_create(
        name = build,
        is_use = True
    )