import axios from 'axios';

const axiosWarpper = axios.create({

    baseURL: 'http://192.168.88.12:8001/api/v1'
});
axiosWarpper.defaults.headers['Content-Type'] = "application/json";

export default axiosWarpper;
