import React from 'react';
import QrReader from "react-qr-reader";
import './styles.css';
import { Redirect } from 'react-router-dom';

export default class ScanQR extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        delay: 1000,
        legacy: true,
        result: "",
        toCheck: false,
      };
      this.handleScan = this.handleScan.bind(this);
      this.toCheck = this.toCheck.bind(this);
    }
    handleScan(data) {
      if (data) {
        this.setState({
          result: data
        });
        this.toCheck()
      }
    }
    handleError(err) {
      console.error(err);
    }
    toCheck = () => {
      if(this.state.result != null){
        console.log("set true");
        this.setState({ toCheck : true });
      }
      
    }

    render() {
      if(this.state.toCheck) {
        return <Redirect to={{ pathname: "/check", state: { property_id: this.state.result }}} />
      }

      return (
        <div style={{ width: "100%", height: "100%" }}>

          <div class="qrcode-data">
            <QrReader
              delay={this.state.delay}
              onError={this.handleError}
              // legacyMode={this.state.legacy}
              onScan={this.handleScan}
              style={{ width: "100%" }}
            />      
            
          </div>
          
        </div>
      );
    }
  }