import React from 'react';
import './styles.css';
import tulogo from './img/tulogo_1.png';
import { Typography } from 'antd';
import { UserOutlined, KeyOutlined } from '@ant-design/icons';
import axiosIn from "./axiosIn";
import { Redirect } from 'react-router-dom'; 

export default class Login extends React.Component {
    constructor() {
        super();
        this.state={
            input: {},
            error: {},
            username: "",
            password: "",
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        let input = this.state.input;
        input[event.target.name] = event.target.value;
        this.setState({input});    
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const header = {
            "Content-Type": "application/json",
        };

        let data = {
            username: this.state.input.username,
            password: this.state.input.password,
        };
        // console.log(data);

        axiosIn
            .post(`/auth/token/`, data, { headers: header })
            .then((res) => {
                console.log(res);
                console.log(res.data);
                if (res.status === 200) {                    
                    localStorage.setItem("accessToken", res.data.access);                    
                    this.setState({ isSuccess: true });
                }
                })
            .catch((error) => {
                console.log(error);
            alert(error);
        });

        if (this.validate()) {
            console.log(this.state);
        
            let input = {};
            input["username"] = "";
            input["password"] = "";
            this.setState({ input: input });
        }
      
    }

    validate() {
        let input = this.state.input;
        let error = {};
        let isValid = true;
    
        if (!input["username"]) {
          isValid = false;
          error["username"] = "* Please enter your username.";
        }
    
        if (!input["password"]) {
          isValid = false;
          error["password"] = "* Please enter your password.";
        }
    
        this.setState({
          error: error,
        });
    
        return isValid;
      }
    

    render(){
        const { Text } = Typography;

        if(this.state.isSuccess) {            
            return (
                <Redirect to="/home" />
            );
        }
        return (
            <div className="login-page">                
                <img src={tulogo} className="login-logo" alt="logo" />
                <h1>ระบบจัดการครุภัณฑ์</h1>
                <form className="login-form">
                    <label>
                        <UserOutlined className="login-icon" />
                        <input 
                            type="text" 
                            name="username"
                            value={this.state.input.username}
                            onChange={this.handleChange}
                        />
                    </label>
                    <Text className="danger-text">{this.state.error.username}</Text>
                    <label>
                        <KeyOutlined className="login-icon" />
                        <input 
                            type="password" 
                            name="password"
                            value={this.state.input.password}
                            onChange={this.handleChange}
                        />
                    </label> 
                    <Text className="danger-text">{this.state.error.password}</Text>                   
                    <input 
                        id="login-btn" 
                        type="submit" 
                        value="เข้าสู่ระบบ"
                        onClick={this.handleSubmit}
                    />
                </form>
                
            </div>
        )
    };
}