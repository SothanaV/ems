import React from 'react';
import './styles.css';
import { Button } from 'antd';
import { Redirect } from 'react-router-dom';
import qrcode_img from './img/qrcode.png'; 
import inuse_img from './img/stillalive.png';


export default class Home extends React.Component {    
    constructor(){
        super();
        this.state = {
            is_staff: false,
        };
        this.toProfile = this.toProfile.bind(this);
    }

    toProfile = (e) => {
        this.setState({ toProfile : true});
    }

    render(){

        if(this.state.toProfile) {
            return (<Redirect to="/profile" />);
        }

        return (
            <div className="home-page">
                <div className="home-top">
                    <p id="header-time">00 : 00</p>
                    <p id="header-home">หน้าแรก</p>
                    <Button className="profile-btn" onClick={this.toProfile}><img alt="Profile button" /></Button>
                </div>
                <div className="home-function">
                    <div className="home-funtion">
                        <Button id="img-btn"><img src={qrcode_img} alt="scan qrcode"/></Button>
                        <Button id="f1-text-btn">แสกนเข้าใช้งาน</Button>
                    </div>
                    <div className="home-funtion">
                        <Button id="img-btn"><img src={inuse_img} alt="scan qrcode"/></Button>
                        <Button id="f2-text-btn">ครุภัณฑ์ที่กำลังใช้งาน</Button>
                    </div>
                </div>
            </div>
        )
    };
}
