import React from 'react';
import './styles.css';
import { Button } from 'antd';
import { Redirect } from 'react-router-dom';
import { LeftOutlined, CheckOutlined } from '@ant-design/icons';
import axiosIn from './axiosIn';
import axiosWarpper from './axiosWarpper';

const accessToken = localStorage.getItem('accessToken');        
const header = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization": `Bearer ${accessToken}`
}
const header2 = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": `Bearer ${accessToken}`
}

export default class Upload extends React.Component {    
    constructor(){
        super();
        this.state = {
            account: {},
            files: [],      
            modal: false,
        };
        this.goBack = this.goBack.bind(this);
        this.onfileChange = this.onfileChange.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
    }
    fileAarray = [];

    goBack = (e) => {
        this.setState({ goBack: true });
    }

    onfileChange = (e) => {
        console.log("state file", e.target.file[0].name);
        this.setState({ selectedFile: e.target.file[0], loaded: 0});
    }

    showFile() {
        for(let i=0; i<this.state.file.length ; i++) {
            let dataFile = this.state.file[i];
            console.log("i:", i, " name:", dataFile.name);
            // <FileList data={dataFile} />
        }
    }

    uploadFile = (event) => {   

        for(let size=0; size < event.target.files.length; size++) {
            
            let file = event.target.files[size];
            this.fileAarray.push(file);
            console.log("push", file);
            if(size === event.target.files.length - 1) {
                // console.log("set true file size",this.state.files[0].name)
                this.setState({ toData: true });
            }
        }
        this.setState({ files : this.fileAarray, haveFile : true, });
        console.log("finally", this.state.files);

    }

    submitFile = (e) => {
        console.log("submit", this.state.files);
        for(let i = 0 ; i<this.state.files.length ;i++) {
            let file = this.state.files[i];
            const formData = new FormData();
            formData.append("file", file);
            // console.log("file", this.state.files[i]);
            axiosWarpper
            .post(`/importdata/`, formData, { headers: header2 })
            .then((res) => {
                console.log("status", res.status);
              if (res.status === 201) {
                // console.log(res.data);
                 // after signing up, set the state to true. This will trigger a re-render
              }
            })
            .catch((error) => {
              // console.log(error.response.data);
              console.log(error.response);
              alert("Please try again");
            });
            console.log("uploading", file.name);
        }
        this.setState({ modal : true });
    }

    showModal() {

        return (
            <div className="bg-modal">           
                <div className="upload-modal">
                    <p>อัพโหลดข้อมูลเสร็จสิ้น</p>
                    <CheckOutlined id="icon-check-page" />
                    <Button onClick={this.goBack}>กลับหน้าหลัก</Button>
                </div>
            </div>
          );
    }

    componentDidMount() {
        axiosIn.get('/account/', { headers: header })
            .then(res => {
                const temp = res.data[0]; // get the data array instead of object                
                this.setState({ account : temp });
            })
            .catch(err => { // log request error and prevent access to undefined state
                console.error(err);
            })
    }

    render(){
        
        if(this.state.goBack) {
            return (<Redirect to="/home" />);
        }

        return (
            <div className="page-bg-style1">
                {this.state.modal ? this.showModal() : null}
                <div className="page-top-bg-style1">
                    <Button id="back-btn2" onClick={this.goBack}><LeftOutlined /></Button>
                    <p id="header-home">ลงทะเบียนครุภัณฑ์</p>
                    <Button className="profile-btn" onClick={this.toProfile}><img alt="Profile button" src={this.state.account.image}  /></Button>
                </div>
                <div className="upload-bg">
                    <div className="upload-box"> 
                        <input type="file" multiple name="file" accept=".csv, .xlsx" onChange={this.uploadFile} />
                        {this.state.toData ? 
                            <div style={{ width: "300px", height: "300px", color: "#000000" }}>
                                <p>รายชื่อไฟล์ที่อัพโหลด</p>
                                {/* <p>{this.state.files[0].name}</p> */}
                                {this.state.files.map(item => <p>{item.name}</p>)}
                            </div>
                            : <div>
                                <p></p>
                            </div>
                        }
                        <button id="upload-btn" onClick={this.submitFile}>อัพโหลดไฟล์</button> 
                    </div>                                 
                </div>
            </div>
        )
    };
}
