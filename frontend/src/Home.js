import React from 'react';
import './styles.css';
import { Button } from 'antd';
import { Redirect } from 'react-router-dom';
import qrcode_img from './img/qrcode.png';
import check_item from './img/check_item.png';
import register_item from './img/register_item.png';
import axiosIn from './axiosIn';

export default class Home extends React.Component {    
    constructor(){
        super();
        this.state = {
            data: {},
            loading: false,
            is_staff2: false,
        };
        this.toProfile = this.toProfile.bind(this);
        this.toCheckItem = this.toCheckItem.bind(this);
        this.toUpload = this.toUpload.bind(this);
        this.toScanQR = this.toScanQR.bind(this);
        this.toInUse = this.toInUse.bind(this);
    }

    toProfile = (e) => {
        this.setState({ toProfile : true });
    }
    toCheckItem = (e) => {
        this.setState({ toCheckItem : true });
    }
    toUpload = (e) => {        
        this.setState({ toUpload : true });
    }
    toScanQR = () => {
        this.setState({ toScanQR : true });
    }
    toInUse = () => {
        this.setState({ toInUse : true });
    }

    componentDidMount(){
        const accessToken = localStorage.getItem('accessToken');        
        const header = {
            "Content-Type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": `Bearer ${accessToken}`
        }
        axiosIn.get('/account/', { headers: header })
            .then(res => {
                const temp = res.data[0]; // get the data array instead of object
                console.log("get data:", temp);
                this.setState({ data: temp });
            })
            .catch(err => { // log request error and prevent access to undefined state
                console.error(err);
            })
        this.setState({ loading: true });
    }

    render(){
        let is_staff = this.state.data.is_staff;

        if(this.state.toProfile) {
            return (<Redirect to="/profile" />);
        }
        if(this.state.toCheckItem) {
            return (<Redirect to="/check_item" />);
        }
        if(this.state.toUpload) {
            return (<Redirect to="/upload_item" />);
        }
        if(this.state.toScanQR) {
            return (<Redirect to="/scanQR" />);
        }
        if(this.state.toInUse) {
            return (<Redirect to="/in_use" />);
        }

        if(!this.state.loading) {
            console.log("first loading:", this.state.loading);
            return (
                <div>
                    <p>Loading...</p>
                </div>
            );
        } else if(is_staff) {
            console.log("at staff => staff:", this.state.data.is_staff, " loading:", this.state.loading);

            return (
                <div className="page-bg-style1">
                    <div className="page-top-bg-style1">
                        <p id="header-time">00 : 00</p>
                        <p id="header-home">หน้าแรก</p>
                        <Button className="profile-btn" onClick={this.toProfile}><img alt="Profile button" src={this.state.data.image}  /></Button>
                    </div>
                    <div className="page-body-bg">
                        <div className="user-detail">
                            <p>ยินดีต้อนรับคุณ {this.state.data.first_name} {this.state.data.last_name}</p>
                        </div>  
                        <div className="home-funtion">
                            <Button id="img-btn" onClick={this.toCheckItem}><img src={check_item} alt="scan qrcode"/></Button>
                            <Button id="f1-text-btn" onClick={this.toCheckItem}>ตรวจสอบครุภัณฑ์</Button>
                        </div>
                        <div className="home-funtion">
                            <Button id="img-btn" onClick={this.toUpload}><img src={register_item} alt="scan qrcode"/></Button>
                            <Button id="f2-text-btn" onClick={this.toUpload}>ลงทะเบียนครุภัณฑ์</Button>
                        </div>
                    </div>
                </div>
            )
        } else if(!is_staff){
            console.log("at user => staff:", this.state.data.is_staff, " loading:", this.state.loading);

            return (
                <div className="page-bg-style1">
                    <div className="page-top-bg-style1">
                        <p id="header-time">00 : 00</p>
                        <p id="header-home">หน้าแรก</p>
                        <Button className="profile-btn" onClick={this.toProfile}><img alt="Profile button" src={this.state.data.image} /></Button>
                    </div>
                    <div className="page-body2-bg">
                        <div className="user-detail">
                            <p>ยินดีต้อนรับคุณ {this.state.data.first_name} {this.state.data.last_name}</p>
                        </div>                    
                        <div className="home-funtion">

                            <Button id="img-btn"  onClick={this.toScanQR}><img src={qrcode_img} alt="scan qrcode"/></Button>
                            <Button id="f1-text-btn"  onClick={this.toScanQR}>แสกนเข้า/ออกใช้งานครุภัณฑ์</Button>
                        </div>
                    </div>
                </div>
            )
        }

        
    };
}
