import React from 'react';
import './styles.css';
import { Redirect } from 'react-router-dom';
import { Button } from 'antd';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';
import axiosIn from './axiosIn';
import ShowTable from './showTable';

const accessToken = localStorage.getItem('accessToken');        
const header = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization": `Bearer ${accessToken}`
}

export default class Check_Item extends React.Component {
    constructor(){
        super();
        this.state = {
            data: {},
            table: {},
            transaction: {},
            havedata: false,
            modal: false,
            form_year: "",
            form_month: "",
            alert: false,
            code: ""
        };
        this.toProfile = this.toProfile.bind(this);
        this.goBack = this.goBack.bind(this);
        this.getOrder = this.getOrder.bind(this);
        this.showModal = this.showModal.bind(this);
        this.setModal = this.setModal.bind(this);
        this.downloadData = this.downloadData.bind(this);
        this.seacrhCode = this.seacrhCode.bind(this);
    }

    toProfile = (e) => {
        this.setState({ toProfile : true});
    }
    goBack = (e) => {
        this.setState({ goBack: true });
    }

    showData() {
        if(this.state.havedata) {
            return (<ShowTable data={this.state.table}/>);
        }
    }
    
    setModal() {
        if(this.state.modal) {
            this.setState({ modal : false });
        } else {
            this.setState({ modal : true });
        }
    }

    seacrhCode(){        
        axiosIn.get(`/equipment/?search=${this.state.code}`, { headers: header })
            .then(res => {
                const temp_table =  res.data;
                this.setState({ 
                    table: temp_table
                });
            })
            .catch(err => { 
                console.error(err);
            })
    }

    downloadData() {
        let data = {
            year: this.state.form_year,
            month: this.state.form_month
        }
        if(this.state.form_year !== "" || this.state.form_month !== "") {
            axiosIn.post('/export-transaction/', data, { headers: header })
                .then(res => {
                    var contentType = 'text/csv';
                    console.log(res);
                    let filename = res.config.url + ".csv";
                    const temp =  res.data;
                    const data = new Blob([temp], {type: contentType});
                    // window.open(URL.createObjectURL(temp));
                    // window.navigator.msSaveOrOpenBlob(data, filename);
                    var a = document.createElement("a"),
                    url = URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function() {
                        document.body.removeChild(a);
                        window.URL.revokeObjectURL(url);  
                    }, 0); 
                })
                .catch(err => { 
                    console.error(err);
                })
            }
    }

    showModal() {

        return (
            <div className="bg-modal">           
                <div className="data-modal">
                    <Button className="close-btn" onClick={this.setModal}><CloseOutlined /></Button>
                    <h1>ดาวน์โหลดข้อมูล</h1>
                    <form name="download">
                        <label>
                            <p>ปี</p>
                            <select name="year" onClick={(event) => { this.setState({ form_year : event.target.value })}}>
                                <option hidden disabled selected value> โปรดระบุ </option>
                                {this.state.transaction.year.map((data, i) => <option value={data}>{data}</option> )}
                            </select>
                        </label>

                        <label>
                            <p>เดือน</p>
                            <select name="month" onClick={(e) => { this.setState({ form_month : e.target.value })}}>
                                <option hidden disabled selected value> โปรดระบุ </option>
                                {this.state.transaction.month.map((data, i) => <option value={data}>{data}</option> )}
                            </select>
                        </label>


                        {this.state.alert ? <p className="alert-text">*โปรดใส่ตัวเลือก</p> : null}
                        
                        <Button className="download-submit" type="submit" onClick={() => {
                            if(this.state.form_year !== "" || this.state.form_month !=="") {
                                this.setState({ alert : false });
                            } else {
                                this.setState({ alert : true });
                            }
                            this.downloadData()
                            
                        }}>ดาวน์โหลดข้อมูล</Button>
                    </form>
                   
                </div>
            </div>
          );
    }

    getOrder = (e) => {
        e.preventDefault();
        axiosIn.get('/equipment/?order={e.target.value}', { headers: header })
            .then(res => {
                const temp_table =  res.data;
                this.setState({ 
                    table: temp_table
                });
            })
            .catch(err => { 
                console.error(err);
            })

        this.showData();
    }    

    componentDidMount(){        
        axiosIn.get('/account/', { headers: header })
                .then(res => {
                    const temp = res.data[0]; // get the data array instead of object                
                    this.setState({ data: temp });
                })
                .catch(err => { // log request error and prevent access to undefined state
                    console.error(err);
                })
        axiosIn.get('/equipment/?order=code', { headers: header })
            .then(res => {
                const temp_table =  res.data;            
                this.setState({ 
                    table: temp_table, 
                    havedata: true
                });
                console.log("Aready have data");
            })
            .catch(err => { 
                console.error(err);
            })
            axiosIn.get('/export-transaction/', { headers: header })
            .then(res => {
                const temp =  res.data;
                this.setState({ 
                    transaction: temp,
                });
            })
            .catch(err => { 
                console.error(err);
            })
    }
    
    render(){    
        if(this.state.toProfile) {
            return (<Redirect to="/profile" />);
        }
        if(this.state.goBack) {
            return (<Redirect to="/home" />);
        }

        return (
            <div className="page-bg-style1">
                <div className="page-top-bg-style1">
                    <Button id="back-btn2" onClick={this.goBack}><LeftOutlined /></Button>
                    <p id="header-home">ตรวจสอบครุภัณฑ์</p>
                    <Button className="profile-btn" onClick={this.toProfile}><img alt="Profile button" src={this.state.data.image}  /></Button>
                </div>
                <div className="body-check-bg" >
                    <p id="body-check-head">ครุภัณฑ์ที่มีอยู่</p>
                    
                    <form className="search-box">
                        <input 
                            id="search-input"
                            placeholder="ป้อนรหัสครุภัณฑ์"
                            type="text"
                            name="code"
                            onChange={(e) => {
                                this.setState({ code : e.target.value });
                            }}
                            />
                        <Button type="submit" id="search-btn" onClick={this.seacrhCode}>ค้นหา</Button>
                    </form>

                    <div className="check-row">
                        <p id="text-check">จัดเรียงตาม</p>
                        <div>
                            <select 
                                className="select-sort" 
                                name="order"
                                onChange={this.getOrder}
                            >
                                <option value="code">รหัสครุภัณฑ์</option>
                                <option value="add_date">วัน-เวลา</option>
                            </select>
                        </div>
                    </div>
                    
                    <div className="show-data-table">
                        {this.showData()}
                    </div>

                    <Button 
                        className="downloade-btn" 
                        onClick={this.setModal}>ดาวน์โหลดข้อมูล</Button>
                    {this.state.modal ? this.showModal() : null}
                </div>
            </div>
        )
    }
}
