import { Table } from 'antd';
import React from 'react';

function ShowTable(props) {
    const columns = [
        {
          title: 'ลำดับ',
          dataIndex: 'id',
          key: 'id',          
        },
        {
          title: 'รหัส',
          dataIndex: 'code',
          key: 'code',
        },
        {
          title: 'ประเภท',
          dataIndex: 'category',
          key: 'category',
        },
        {
            title: 'รายละเอียด',
            dataIndex: 'status',
            key: 'status',
          },
          {
            title: 'วันที่',
            dataIndex: 'add_date',
            key: 'add_date',
          },
      ];

    return (
        <div id="show-table">
            <Table dataSource={props.data} columns={columns} pagination={false} style={{ width: "100%" }} />
        </div>
    );
}

export default ShowTable;