import React from 'react';
import './styles.css';
import { Button } from 'antd';
import { Redirect } from 'react-router-dom';
import { LeftOutlined } from '@ant-design/icons';
import axiosIn from './axiosIn';

export default class Profile extends React.Component {
    constructor(){
        super();
        this.state = {
            data: {},
        };
        this.logout = this.logout.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    goBack = (e) => {
        this.setState({ goBack: true });
    }

    logout = (e) => {
        localStorage.clear();        
        this.setState({ isLogout: true});
    }

    componentDidMount(){
        const accessToken = localStorage.getItem('accessToken');        
        const header = {
            "Content-Type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": `Bearer ${accessToken}`
        }
        axiosIn.get('/account/', { headers: header })
            .then(res => {
                const data = res.data[0]; // get the data array instead of object
                this.setState({ data: data});
            })
            .catch(err => { // log request error and prevent access to undefined state
                console.error(err);
            })
    }
    
    render(){
        let status = "";
        if(this.state.data.is_staff) {
            status = "ผู้ดูแล";
        } else {
            status = "ผู้ใช้ชั่วคราว";
        }

        if(this.state.isLogout) {
            return (<Redirect to="/login" />);
        }
        if(this.state.goBack) {
            return (<Redirect to="/home" />);
        }

        return (
            <div className="profile-page">
                <Button className="back-btn" onClick={this.goBack}><LeftOutlined /></Button>
                <img className="profile-avatar" src={this.state.data.image} alt="Profile pic"/>
                <p>{this.state.data.first_name} {this.state.data.last_name}</p>
                <p>{status}</p>
                <Button className="profile-logout-btn" onClick={this.logout}>ออกจากระบบ</Button>
            </div>
        )
    };
}