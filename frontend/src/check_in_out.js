import React, { useState } from 'react';
import './styles.css';
import axiosIn from './axiosIn';
import { Button } from 'antd';
import { Redirect } from 'react-router-dom';
import { CheckOutlined } from '@ant-design/icons';

const accessToken = localStorage.getItem('accessToken');        
const header = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": `Bearer ${accessToken}`
}

const CheckInOut = (props) => { 
    let roomid = props.location.state.property_id;
    
    const [backHome, setBackHome ] = useState(false);
    const [in_use, setInuse] = useState([]);
    const [room, setRoom] = useState([]);
    const [id, setId] = useState({room: "", state: ""});
    const [havedata, setHavedata] = useState(false);
    const [notiShow, setNotiShow] = useState(false);

    if(!havedata) {               
        axiosIn.get(`/room/${roomid}/`, { headers: header })
        .then(res => {
            const temp = res.data;
            console.log(temp);
            setRoom(temp);
            setInuse(temp.in_use);
            console.log(in_use);
            if(in_use) {
                setId({ 
                    room: temp.id,
                    state: "out"
                });     
                setHavedata(true);        
            }else {
                setId({ 
                    room: temp.id,
                    state: "in"
                });
                setHavedata(true); 
            }
            
            console.log("id", id);
            
        })
        .catch(err => { // log request error and prevent access to undefined state
            console.error(err);
        })
    }

    if(backHome) {
        return(<Redirect to="/home" />);
    }

    return (
        <div className="check-bg">
            <div className={`noti-action ${notiShow ? "notiShow" : "notiHide"}`}>
                <div className="noti-content">
                    <p>บันทึกข้อมูลสำเร็จ</p>                    
                    <CheckOutlined id="icon-check-page" />
                    <Button onClick={ () => { setBackHome(true) }} >กลับไปหน้าหลัก</Button>
                </div>
                
            </div>
            <div className="checkin-box">  
                <p>รายละเอียด</p>          
                <p>อาคาร:{room.building}</p>
                <p>ห้อง:{room.name}</p>
                <p>สถานะ: {in_use ? "กำลังใช้งาน" : "พร้อมใช้งาน" }</p>
                {in_use ? 
                    <div>
                        <p>คุณต้องการลงชื่อออกใช้งานใช่หรือไม่</p>
                        <div className="check-in-bottom-btn">
                            <Button id="back-home-btn" onClick={ () => { setBackHome(true) }
                            } >ย้อนกลับ</Button>
                            <Button id="submit-out-btn" onClick={() => {
                                console.log("send", id);
                                axiosIn.post('/transactions/', id,{ headers: header })
                                    .then(() => 
                                        setNotiShow(true)
                                    )                        
                                    .catch(err => { // log request error and prevent access to undefined state
                                        console.error(err);
                                    })
                                
                                
                            }}>เลิกใช้งาน</Button>
                        </div>
                    </div>
                    :
                    <div>
                        <p>คุณต้องการลงชื่อเข้าใช้งานใช่หรือไม่</p>
                        <div className="check-in-bottom-btn">
                            <Button id="back-home-btn" onClick={ () => { setBackHome(true) }
                            } >ย้อนกลับ</Button>
                            <Button id="submit-in-btn" onClick={() => {
                                console.log("send", id);
                                axiosIn.post('/transactions/', id,{ headers: header })
                                    .then(() => 
                                        setNotiShow(true)
                                    ) 
                                    .catch(err => { // log request error and prevent access to undefined state
                                        console.error(err);
                                    })
                                
                            }}>เข้าใช้งาน</Button>
                        </div>
                    </div>

                }

                
            </div>
        
        </div>
        );
    
}

export default CheckInOut;