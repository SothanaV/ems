import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Login from './Login';
import Home from './Home';
import Profile from './Profile';
import Inuse from './In_use';
import CheckItem from './check_item';
import UploadItem from './Upload';
import ScanQR from './scanQR';
import Check from './check_in_out';

function EMSRouter() {
  return(
    <div>
      <Route exact path="/login" component={Login} />
      <Route exact path="/" component={Login} />
      <Route exact path="/home" component={Home} />
      <Route exact path="/profile" component={Profile} />
      <Route exact path="/in_use" component={Inuse} />
      <Route exact path="/check_item" component={CheckItem} />
      <Route exact path="/upload_item" component={UploadItem} />
      <Route exact path="/scanQR" component={ScanQR} />
      <Route exact path="/check" component={Check} />
    </div>
  );
}

ReactDOM.render(
  <BrowserRouter>
    <EMSRouter />
  </BrowserRouter>,
  // <React.StrictMode>
  //   <Login />
  // </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
